CPPFLAGS = -MMD
CC = gcc
CFLAGS = `pkg-config --cflags sdl gtk+-3.0` -Wall -Wextra -std=c99 -g
LDFLAGS =
LDLIBS = `pkg-config --libs gtk+-3.0` -lSDL -lSDL_image -lm

IMAGE-SRC = $(filter-out image/nogui.c image/main.c, $(wildcard image/*.c))
NN-SRC = $(filter-out nn/main.c, $(wildcard nn/*.c))

SRC-MAIN = ${IMAGE-SRC} ${NN-SRC} interface.c train_load.c nn.c main.c
OBJ-MAIN = ${SRC-MAIN:.c=.o}

SRC-NOGUI = ${IMAGE-SRC} ${NN-SRC} train_load.c nn.c nogui.c
OBJ-NOGUI = ${SRC-NOGUI:.c=.o}

DEP = ${SRC-MAIN:.c=.d} ${SRC-NOGUI:.c=.d}

all: main nogui

main: ${OBJ-MAIN}

nogui: ${OBJ-NOGUI}

.PHONY: clean

clean:
	${RM} ${OBJ-MAIN}   # remove object files
	${RM} ${OBJ-NOGUI}  # remove object files
	${RM} ${DEP}   	    # remove dependency files
	${RM} main          # remove main program
	${RM} nogui
	${RM} *.gch         # remove precompiled headers

-include ${DEP}

# END
