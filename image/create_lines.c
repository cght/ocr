#include <err.h>
#include "SDL/SDL.h"
#include "SDL/SDL_image.h"

#include "pixel_operations.h"
#include "tools.h"


/*
** draw_line
** draw a horizontal line between two points on an image
** input:
**  - pointer on image
**  - two x coordinates and y coordinate
**  - color to draw line with
** output:
** image pointer
*/
void draw_line(SDL_Surface *image,
                      int x1, int x2, int y, Uint32 pixel) {
    x1 = clamp(x1, 0, image->w - 1);
    x2 = clamp(x2, 0, image->w - 1);
    y = clamp(y, 0, image->h - 1);
    if(x1 > x2){
        int tmp = x1;
        x1 = x2;
        x2 = tmp;
    }
    for(int x = x1; x < x2; x++) {
        put_pixel(image, x, y, pixel);
    }
}

/*
** drawn_column
** draw vertical line (column) between two points on an image
** input:
**  - pointer on image
**  - x coordinate and two y coordinates
**  - color to draw line with
** output:
** image pointer
*/
void drawn_column(SDL_Surface *image,
                        int x, int y1, int y2, Uint32 pixel) {
    x = clamp(x, 0, image->w - 1);
    y1 = clamp(y1, 0, image->h - 1);
    y2 = clamp(y2, 0, image->h - 1);
    if(y1 > y2){
        int tmp = y1;
        y1 = y2;
        y2 = tmp;
    }
    for(int y = y1; y < y2; y++) {
        put_pixel(image, x, y, pixel);
    }
}
