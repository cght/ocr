#ifndef CREATELINES_H
#define CREATELINES_H

#include "SDL/SDL.h"
#include "SDL/SDL_image.h"

void draw_line(SDL_Surface *image, int x1, int x2, int y, Uint32 pixel);

void drawn_column(SDL_Surface *image, int x, int y1, int y2, Uint32 pixel);

#endif
