#include <stdio.h>
#include <err.h>

#include "pixel_operations.h"
#include "create_lines.h"
#include "tools.h"
#include "sdl_management.h"

#define false 0
#define true 1
#define NB_PIXEL_LINE 13
#define NB_PIXEL_COLUMN 13
#define BLACK_MAX_VALUE 50
#define MAX_SPACE 2


/*
** lines_with_char
** return a char* specifying lines that start and end the text block
** between x1 and x2
*/
char *lines_with_char(SDL_Surface *image, int x1, int x2) {

    char *res = (char*) malloc(image->h * sizeof(char));
    if (!res) {
        errx(1, "Not enough memory to malloc res in lines_with_char");
    }
    for(int i = 0; i < image->h; i++) {
        res[i] = 0;
    }
    char isText = false;

    for(int y = 0; y < image->h; y++) {
        char hasBlack = false;
        for(int x = x1; x < x2; x++) {
            Uint32 pixel = get_pixel(image, x, y);
            Uint8 r, g, b;
            SDL_GetRGB(pixel, image->format, &r, &g, &b);
            if(r < BLACK_MAX_VALUE) {
                hasBlack = true;
            }
        }
        if(hasBlack && !isText) {
            isText = true;
            res[y] = 1;
        } else if(!hasBlack && isText) {
            isText = false;
            res[MAX(0, y - 1)] = 2;
        }
    }

    return res;
}



/*
** Takes an image, 2 integers and
** return a char* with the columns with black pixels between the 2 intergers
*/
char *column_with_char(SDL_Surface *image, int y1, int y2) {

    char *res = (char*) malloc(image->w * sizeof(char));
    if (!res) {
        errx(1, "Not enough memory to malloc res in column_with_char");
    }

    // set every value of res to 0
    for(int i = 0; i < image->w; i++) {
        res[i] = 0;
    }

    char isText = false;

    //starts checking for black pixels
    for(int x = 0; x <= image->w; x++) {
        char hasBlack = false;
        for(int y = y1; y < y2; y++) {
            Uint32 pixel = get_pixel(image, x, y);
            Uint8 r, g, b;
            SDL_GetRGB(pixel, image->format, &r, &g, &b);
            if(r < BLACK_MAX_VALUE) {
                hasBlack = true;
            }
        }
        if(hasBlack) {
            if(!isText) {
                res[x] = 1; //sets res[x] to 1 to tell that text starts here
                isText = true;
            }
        } else {
            if(isText) {
                isText = false;
                res[x] = 2; //sets res[x] to 2 to tell that text ends here
            }
        }
    }

    return res;
}


/*
** takes an image and 4 intergers,
** returns a tupple (int*) with 2 values
** representing the upper and lower boundaries of the character
** between the 2 points represented by the 4 intergers
*/
int *char_height(SDL_Surface *image, int x1, int x2, int y1, int y2) {
    int *res = (int*) malloc(2 * sizeof(int));
    if (!res) {
        errx(1, "Not enough memory to malloc res in char_height");
    }
    res[0] = y2;
    res[1] = y1;

    for(int x = x1; x <= x2; x++) {

        for(int y = y1; y <= y2; y++) {
            Uint32 pixel = get_pixel(image, x, y);
            Uint8 r, g, b;
            SDL_GetRGB(pixel, image->format, &r, &g, &b);
            if(r < BLACK_MAX_VALUE && res[0] >= y) {
                res[0] = y;
                break;
            }
        }
        for(int y = y2; y >= y1 ; y--) {
            Uint32 pixel = get_pixel(image, x, y);
            Uint8 r, g, b;
            SDL_GetRGB(pixel, image->format, &r, &g, &b);
            if(r < BLACK_MAX_VALUE && res[1] <= y) {
                res[1] = y;
                break;
            }
        }
    }

    return res;
}

/*
** takes an image, 2 coordinates, and char specifying a res
** to add to the Character
** returns a pointer to the built struct Character
*/
struct Character *create_char(SDL_Surface *image,
                              size_t x1,
                              size_t x2,
                              size_t y1,
                              size_t y2,
                              char is_end_line) {
    struct Character *c = malloc(sizeof(struct Character));
    if (!c) {
        errx(1, "Not enough memory to malloc c in create_char");
    }

    c->image = image;

    c->x1 = x1;
    c->x2 = x2;
    c->y1 = y1;
    c->y2 = y2;

    if (is_end_line)
        c->res = '\n';
    else
        c->res = 0;

    return c;
}

/*
** take a struct Character
** returns a char* list to be read by the NN
*/
char *get_image_data(struct Character *character) {
    char *res = malloc(NB_PIXEL_CHAR * sizeof(char));
    if (!res) {
        errx(1, "Not enough memory to malloc res in create_char");
    }
    Uint8 r, g, b;
    Uint32 pixel;
    SDL_Rect rect;
    rect.x = character->x1;
    rect.y = character->y1;
    rect.w = character->x2 - character->x1 + 1;
    rect.h = character->y2 - character->y1 + 1;

    SDL_Surface *img_crop = // create image of crop size
        SDL_CreateRGBSurface(SDL_HWSURFACE,
                             rect.w,
                             rect.h,
                             character->image->format->BitsPerPixel,
                             0,
                             0,
                             0,
                             0);

    // copy image
    SDL_BlitSurface(character->image, &rect, img_crop, NULL);
    // resize image
    img_crop = resize_image(img_crop, NB_PIXEL_COLUMN, NB_PIXEL_LINE, true);

    for (int y = 0; y < img_crop->h; y++) {
        for (int x = 0; x < img_crop->w; x++) {
            pixel = get_pixel(img_crop, x, y);
            SDL_GetRGB(pixel, img_crop->format, &r, &g, &b);
            if(r < BLACK_MAX_VALUE) {
                res[x + y * NB_PIXEL_COLUMN] = 1;
            }
            else {
                res[x + y * NB_PIXEL_COLUMN] = 0;
            }
        }
    }

    return res;
}

double space_average(struct CharList *list) {
    int space = 0;
    int nb_space = 0;
    for (; list->next; list = list->next) {
        if (list->value->res != '\n' && list->next->value->res != '\n') {
            space += list->next->value->x1 - list->value->x2;
            nb_space++;
        }
    }
    return (double) space / (double) nb_space;
}

void add_space(struct CharList *list) {
    if (list) {
        double space_mean = space_average(list);

        for (struct CharList *l = list; l->next; l = l->next) {
            if (l->value->res != '\n' &&
                l->next->value->res != '\n' &&
                l->next->value->x1 - l->value->x2 > space_mean + MAX_SPACE) {
                    struct CharList *space = malloc(sizeof(struct CharList));
                    if (!space)
                    {
                        errx(1, "add_space: malloc failed on space");
                    }
                    space->next = l->next;
                    l->next = space;
                    l = space->next;

                    space->value = malloc(sizeof(struct Character));
                    if (!space->value)
                    {
                        errx(1, "add_space: malloc failed on space->value");
                    }
                    space->value->res = ' ';
                    space->value->x1 = 0;
                    space->value->x2 = 0;
                    space->value->y1 = 0;
                    space->value->y2 = 0;
                    space->value->image = l->value->image;
                    continue;
            }
        }
    }
}

/*
** Detect the text from the 1st iamge
** Frame it in the 2nd image (a copy of the 1st)
*/
struct CharList *text_framing(SDL_Surface *image, SDL_Surface *image_GUI) {
    struct CharList *res = NULL;
    int startLine = 0;
    Uint32 pixel;

    char *lineWithChar = lines_with_char(image, 0, image->w);

    for(int y = 0; y < image->h; y++) {
        if(lineWithChar[y] == 2) {
            char *column = column_with_char(image, startLine, y);
            int startColumn = -1;
            int endColumn = image->w;
            int i = 0;
            for(; i < image->w; i++) {
                if(column[i] == 1) {
                    startColumn = i;
                    break;
                }
            }
            int charStart = 0;

            //character framing
            for(; i < image->w; i++) {
                if(column[i] == 1) {
                    charStart = i;
                }
                else if(column[i] == 2) {
                    endColumn = i;
                    int *charLim = char_height(image,
                                              charStart,
                                              endColumn,
                                              startLine,
                                              y);
                    pixel = SDL_MapRGB(image->format, 0, 0, 0);

                    //update char list
                    res = add_to_list(res,
                                      create_char(image,
                                                  charStart,
                                                  i,
                                                  charLim[0],
                                                  charLim[1],
                                                  false));

                    //graphical display
                    drawn_column(image_GUI,
                                 charStart,
                                 charLim[0],
                                 charLim[1],
                                 pixel);
                    drawn_column(image_GUI, i, charLim[0], charLim[1], pixel);
                    draw_line(image_GUI, charStart, i, charLim[0], pixel);
                    draw_line(image_GUI, charStart, i, charLim[1], pixel);

                    free(charLim);
                }
            }

            //Graphical text framing
            pixel = SDL_MapRGB(image->format, 0, 0, 255);
            draw_line(image_GUI, startColumn, endColumn, startLine, pixel);

            pixel = SDL_MapRGB(image->format, 255, 255, 0);
            draw_line(image_GUI, startColumn, endColumn, y, pixel);

            pixel = SDL_MapRGB(image->format, 255, 0, 0);
            drawn_column(image_GUI, startColumn, startLine, y, pixel);

            pixel = SDL_MapRGB(image->format, 0, 255, 0);
            drawn_column(image_GUI, endColumn, startLine, y, pixel);

            res = add_to_list(res,
                              create_char(image,
                                          0,
                                          0,
                                          0,
                                          0,
                                          true));

            free(column);
        }
        if(lineWithChar[y] == 1) {
            startLine = y;
        }
    }
    free(lineWithChar);

    res = revert_list(res);
    add_space(res);

    return res;
}
