#ifndef DETECT_CHAR_H
#define DETECT_CHAR_H

#include "SDL/SDL.h"
#include "SDL/SDL_image.h"

#include "tools.h"

struct CharList *text_framing(SDL_Surface *image, SDL_Surface* image_GUI);

char *get_image_data(struct Character *character);

#endif
