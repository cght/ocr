#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <err.h>
#include "SDL/SDL.h"
#include "SDL/SDL_image.h"

#include "tools.h"
#include "sdl_management.h"
#include "pixel_operations.h"
#include "detect_char.h"
#include "preprocessing.h"


int main(int argc, char** argv) {
    if(argc < 2) {
        errx(1, "Please enter a valid image.");
    }
    char* param = argv[1];

    // load the image
    SDL_Surface* image = Load(param);
    SDL_Surface* screen_surface = display_image(image);

    // preprocessing
    printf("Press a key to Binarize\n");
    wait_for_keypressed();
    blackwhite(image);
    update_surface(screen_surface, image);

    printf("Press a key to Blur\n");
    wait_for_keypressed();
    image = gaussian_blur(image, 1);
    printf("Gaussian Blur : Done\n");
    update_surface(screen_surface, image);

    printf("Press a key to rebinarize\n");
    wait_for_keypressed();
    binarize(image, 127);
    update_surface(screen_surface, image);
    printf("Rebinarization : Done\n");

/*    printf("Press a key to reduct noise\n");
    wait_for_keypressed();
    image = noise_reduction(image);
    printf("Noise Reduction : Done\n");
    update_surface(screen_surface, image);
*/
    // read the image
    printf("Press a key to detect characters\n");
    wait_for_keypressed();
    SDL_Surface *image_cp = SDL_CreateRGBSurface(0, image->w, image->h, 32, 0, 0, 0, 0);
    SDL_BlitSurface(image, NULL, image_cp, NULL);
    struct CharList *charList = text_framing(image, image_cp);
    for (; charList; charList = charList->next) {
        char *ptr = get_image_data(charList->value);
        print_char_matrix(ptr, 13);
        printf("\n  ====================================");
        free(ptr);
    }
    printf("\n");
    free_list(charList);
    update_surface(screen_surface, image_cp);

    // free memory and closing the program
    wait_for_keypressed();
    SDL_FreeSurface(screen_surface);
    SDL_FreeSurface(image);
    SDL_FreeSurface(image_cp);
    SDL_Quit();
    return 0;
}
