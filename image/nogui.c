#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <err.h>
#include "SDL/SDL.h"
#include "SDL/SDL_image.h"

#include "tools.h"
#include "sdl_management.h"
#include "pixel_operations.h"
#include "detect_char.h"
#include "preprocessing.h"

/*
** alternative main for continuous integration purposes
*/
int main(int argc, char** argv) {
    if(argc < 2) {
        errx(1, "Please enter a valid image.");
    }
    char *param = argv[1];

	/*sleep(2);
    printf("Segmentation fault (core dumped)\n");
	sleep(10);*/

    // loading the image
    SDL_Surface *image = Load(param);
    printf("Image loaded.\n");

    // preprocessing
    blackwhite(image);
    image = gaussian_blur(image, 1);
    binarize(image, 127);

    // reading the image
    SDL_Surface *image_cp = SDL_CreateRGBSurface(0, image->w, image->h, 32, 0, 0, 0, 0);
    SDL_BlitSurface(image, NULL, image_cp, NULL);
    struct CharList *charList = text_framing(image, image_cp);
    for (; charList; charList = charList->next) {
        char *ptr = get_image_data(charList->value);
        print_char_matrix(ptr, 16);
        printf("\n=================================================================");
        free(ptr);
    }
    free_list(charList);

    // freeing memory and closing the program
    SDL_FreeSurface(image);
    SDL_FreeSurface(image_cp);
    SDL_Quit();
    return 0;
}
