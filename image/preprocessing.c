#include <err.h>
#include "SDL/SDL.h"
#include "SDL/SDL_image.h"
#include "pixel_operations.h"
#include "sdl_management.h"

/*
** average_light
** calculate average luminosity of image_surface
*/
int average_light(SDL_Surface* image_surface)
{
    int width = image_surface->w;
    int height = image_surface->h;

    int light_sum = 0;
    int average_light = 0;
    int number_of_pixels = (width * height);

    Uint32 pixel;
    Uint8 r, g, b;
    Uint8 average;

    for (int i = 0; i < width; i++)
    {
        for (int j = 0; j < height; j++)
        {
            // get value of pixel
            pixel = get_pixel(image_surface, i, j);
            // get RGB value of pixel
            SDL_GetRGB(pixel, image_surface->format, &r, &g, &b);
            average = (r + g + b) / 3;
            // sum every pixel's RGB value
            light_sum += average;
        }
    }

    average_light = light_sum / number_of_pixels;

    return average_light;
}


/*
** binarize
** binarize image_surface with threshold thresh
*/
void binarize(SDL_Surface* image_surface, int thresh)
{
    int width = image_surface->w;
    int height = image_surface->h;

    Uint32 pixel;
    Uint8 r, g, b;
    Uint8 average;

    for (int i= 0; i < width; i++)
    {
        for (int j = 0; j < height; j++)
        {
            pixel = get_pixel(image_surface, i, j);
            SDL_GetRGB(pixel, image_surface->format, &r, &g, &b);
            average = (r + g + b) / 3;
            if (average <= thresh)
                average = 0;
            else
                average = 255;

            pixel = SDL_MapRGB(image_surface->format, average, average, average);
            put_pixel(image_surface, i, j, pixel);
        }
    }
}


/*
** blackwhite
** apply a different threshold according to average light of image_surface
*/
void blackwhite(SDL_Surface* image_surface)
{
    int AverageLight = average_light(image_surface);

    // bright image
    if (AverageLight >= 200)
        binarize(image_surface, 200);

    // normal image
    else if (AverageLight >= 127)
        binarize(image_surface, 100);

    // dark image
    else
        binarize(image_surface, 76);
}


/*
** gaussian_blur
** apply pseudo-gaussian blur on image_surface to reduce noise
** blur uses radius to blur in a square around each pixel
*/
SDL_Surface* gaussian_blur(SDL_Surface* image_surface, int radius)
{
    int width = image_surface->w;
    int height = image_surface->h;

    // only red canal is used since image is binarized
    int r_sum;
    int total;

    SDL_Surface *image_out = SDL_CreateRGBSurface(
                                            SDL_HWSURFACE,
                                            width,
                                            height,
                                            image_surface->format->BitsPerPixel,
                                            0,
                                            0,
                                            0,
                                            0);
    Uint32 pixel;
    Uint8 r, g, b;

    for (int x = 0; x < width; x++)
    {
        for (int y = 0; y < height; y++)
        {
            total = 0;
            r_sum = 0;

            // loop over square of pixels in radius around pixel
            for (int lx = x - radius; lx < (x + radius); lx++)
            {
                for (int ly = y - radius; ly < (y + radius); ly++)
                {
                    if (lx < 0 || lx >= width || ly < 0 || ly >= height)
                        continue;

                    // get pixel in local square
                    pixel = get_pixel(image_surface, lx, ly);
                    SDL_GetRGB(pixel, image_surface->format, &r, &g, &b);

                    r_sum += r;
                    total++;
                }
            }

            r = r_sum / total;

            // set output image to average
            pixel = SDL_MapRGB(image_surface->format, r, r, r);
            put_pixel(image_out, x, y, pixel);
        }
    }
    SDL_FreeSurface(image_surface);
    return image_out;
}
