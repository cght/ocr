#ifndef PREPROCESSING_H
#define PREPROCESSING_H

#include <stdlib.h>
#include "SDL/SDL.h"

int average_light(SDL_Surface* image_surface);

void binarize (SDL_Surface* image_surface, int thresh);

void blackwhite(SDL_Surface* image_surface);

SDL_Surface* gaussian_blur(SDL_Surface* image_surface, int radius);

#endif
