#include <err.h>
#include "SDL/SDL.h"
#include "SDL/SDL_image.h"

#include "pixel_operations.h"

/*
** init_sdl
** initialize SDL
*/
void init_sdl() {
    if (SDL_Init(SDL_INIT_VIDEO) == -1)
        errx(1,"Could not initialize SDL: %s.\n", SDL_GetError());
}

/*
** load_image
** load image from file path
*/
SDL_Surface* load_image(char *path) {
    SDL_Surface *img;

    img = IMG_Load(path);
    if (!img)
        return NULL;

    return img;
}

/*
** display_image
** display img on an SDL surface
*/
SDL_Surface* display_image(SDL_Surface *img) {
    SDL_Surface *screen;

    // Set the window to the same size as the image
    screen = SDL_SetVideoMode(img->w, img->h, 0, SDL_SWSURFACE|SDL_ANYFORMAT);

    if (screen == NULL) {
        errx(1, "Couldn't set %dx%d video mode: %s\n",
             img->w, img->h, SDL_GetError());
    }

    // Warns if the image and the screen are NULL
    if(SDL_BlitSurface(img, NULL, screen, NULL) < 0)
        warnx("BlitSurface error: %s\n", SDL_GetError());

    SDL_UpdateRect(screen, 0, 0, img->w, img->h);

    return screen;
}

/*
** wait_for_keypressed
** hang until any keyboard key is pressed
*/
void wait_for_keypressed() {
    SDL_Event event;

    // Wait for a key to be down.
    do {
        SDL_PollEvent(&event);
    } while(event.type != SDL_KEYDOWN);

    // Wait for a key to be up.
    do {
        SDL_PollEvent(&event);
    } while(event.type != SDL_KEYUP);
}

/*
** Load
** initialize SDL and load file as image
** return loaded image as SDL_Surface
*/
SDL_Surface* Load(char *file) {
    SDL_Surface *image_surface;

    init_sdl();

    image_surface = load_image(file);

    return image_surface;
}

/*
** resize_image
** resize image to new_width and new_height
** return new surface
*/
SDL_Surface *resize_image(SDL_Surface *image,
                          int new_width,
                          int new_height,
                          char need_free) {
    SDL_Surface *res = SDL_CreateRGBSurface(SDL_HWSURFACE,
                                            new_width,
                                            new_height,
                                            image->format->BitsPerPixel,
                                            0,
                                            0,
                                            0,
                                            0);
    SDL_SoftStretch(image, NULL, res, NULL);
    if (need_free)
        SDL_FreeSurface(image);
  return res;
}
