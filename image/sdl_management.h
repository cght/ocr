#ifndef SDL_MANAGEMENT_H
#define SDL_MANAGEMENT_H

#include "SDL/SDL.h"
#include "SDL/SDL_image.h"

SDL_Surface *Load(char *fich);

SDL_Surface *display_image(SDL_Surface* img);

void wait_for_keypressed();

void SDL_FreeSurface(SDL_Surface* image);

SDL_Surface *resize_image(
    SDL_Surface *image,
    int new_width,
    int new_height,
    char need_free
);

#endif
