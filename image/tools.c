#include <stdlib.h>
#include <err.h>

#include "tools.h"

#define MAT_SIZE 169 // sizeof(mat) / sizeof(char)

/*
** add_to_list
** add character to charList and return updated list
** inputs:
** - char list to update
** - character to add
*/
struct CharList *add_to_list(struct CharList *charList,
                             struct Character *character) {
    struct CharList *tmp = malloc(sizeof(struct CharList));
    if (!tmp)
    {
        errx(1, "add_to_list: malloc failed on tmp");
    }
    tmp->value = character;
    tmp->next = charList;
    return tmp;
};

/*
** revert_list
** reverse order of list
*/
struct CharList *revert_list(struct CharList *list) {
    if (list) {
        struct CharList *prev = NULL;
        struct CharList *next = NULL;
        list = list->next;
        while (list) {
            next = list->next;
            list->next = prev;
            prev = list;
            list = next;
        }
        return prev;
    }
    return NULL;
}

/*
** length_list
** return length of list
*/
size_t length_list(struct CharList *list) {
    size_t res = 0;
    for(; list; list = list->next)
        res++;
    return res;
}

/*
** free_list
** free list and containing data
*/
void free_list(struct CharList *char_list) {
    if (char_list) {
        struct CharList *prev = char_list;
        if (char_list->value)
            free(char_list->value);
        for (char_list = char_list->next;
             char_list;
             char_list = char_list->next)
        {
            if (prev)
                free(prev);
            if (char_list->value)
                free(char_list->value);
            prev = char_list;
        }
    }
}

/*
** print_char_matrix
** print properly formatted matrix
** inputs:
** - matrix to print
** - number of characters per line in matrix
*/
void print_char_matrix(char mat[], size_t nbColumn) {
    for (size_t i = 0; i < MAT_SIZE ; i++) {
        if (i % nbColumn == 0) {
            printf("\n");
        }
        if (mat[i]) {
            printf(" # ");
        }
        else {
            printf("   ");
        }

    }
}

/*
** put_in_matrix
** add element to matrix at given position
** inputs:
** - matrix
** - number of characters per line
** - element to add
** - column coordinate
** - line coordinate
*/
void put_in_matrix(char mat[],
                   size_t nbColumn,
                   char elem,
                   size_t column,
                   size_t line) {
    mat[column + line * nbColumn] = elem;
}

/*
** get_in_matrix
** return element at given position in matrix
** inputs:
** - matrix
** - number of characters per line
** - column coordinate
** - line coordinate
*/
char get_in_matrix(char mat[], size_t nbColumn, size_t column, size_t line) {
    return mat[column + line * nbColumn];
}

/*
** clamp
** restrain x value to interval [min,max]
*/
int clamp(int x, int min, int max) {
    if (x < min) {
        return min;
    }
    if (x > max) {
        return max;
    }
    return x;
}
