#ifndef TOOLS_H
#define TOOLS_H

#include <stdio.h>
#include "SDL/SDL.h"
#include "SDL/SDL_image.h"
#include <gtk/gtk.h>

// input format for NN: 13 x 13 = 169
#define NB_PIXEL_CHAR 169

struct Character {
    SDL_Surface *image;
    size_t x1, x2, y1, y2;

    // character detected by nn
    // value is empty when created
    // filled later by get_nn_output
    char res;
};

// dynamic list of character matrices
struct CharList {
    struct CharList *next;
    struct Character *value;
};

struct ImageInfo {
    // path to image file
    gchar *file_path;
    // path to nn save
    gchar *nn_file;

    GtkAdjustment *nn_adj;
    GtkScale *nn_scale;
    GtkDrawingArea *draw_area;
    GtkAdjustment *adjustment;
    GtkButton *binarize_bt;
    GtkButton *soft;
    GtkButton *strong;
    SDL_Surface *image;
    // copy of image to be displayed in gui
    SDL_Surface *image_framed;
    int guassian_blur_radius;
    int binarize_threshold;
    int nn_epochs;
};

struct CharList *add_to_list(
    struct CharList *charList,
    struct Character *character
);

struct CharList *revert_list(struct CharList *list);

size_t length_list(struct CharList *list);

void free_list(struct CharList *char_list);

void print_char_matrix(char mat[], size_t nbColumn);

void put_in_matrix(
    char mat[],
    size_t nbColumn,
    char elem,
    size_t column,
    size_t line
);

char get_in_matrix(char mat[], size_t nbColumn, size_t column, size_t line);

int clamp(int x, int min, int max);

#endif
