#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <err.h>
#include "SDL/SDL.h"
#include "SDL/SDL_image.h"
#include <gtk/gtk.h>

#include "image/tools.h"
#include "image/sdl_management.h"
#include "image/pixel_operations.h"
#include "image/detect_char.h"
#include "image/preprocessing.h"
#include "nn.h"

#define true 1
#define false 0
#define MAX_IMAGE_HEIGHT 720
#define MAX_IMAGE_WIDTH 1280

/*
** display error in a new window
** when the selected file is not in the right format
*/
void show_no_file_err() {
    GtkWidget *dialog = gtk_message_dialog_new_with_markup(
            NULL,
            GTK_DIALOG_MODAL,
            GTK_MESSAGE_INFO,
            GTK_BUTTONS_OK,
            "<b>No valid file selected.</b>\n\nPlease select a valid file.");
    gtk_dialog_run(GTK_DIALOG(dialog));
    gtk_widget_destroy(dialog);
}

/*
** display the detected string in a new window
**
** inputs :
** - pointer on charlist that contains the results
*/
void show_end_string(struct CharList *char_list) {
    // alloc with + 1 for \0 at end of string
    gchar *text = malloc(sizeof(char) * (length_list(char_list) + 1));
    if (!text) {
        errx(1, "show_end_string: malloc failed on text");
    }

    size_t i = 0;
    for (; char_list; char_list = char_list->next) {
        text[i] = char_list->value->res;
        i++;
    }
    text[i] = 0;
    free_list(char_list);

    // save text
    FILE *f = fopen("text.txt", "w+");
    for (size_t j = 0; j < i; j++) {
        fputc (text[j], f);
    }
    fclose(f);

    // show text
    GtkWidget *dialog = gtk_message_dialog_new_with_markup(
            NULL,
            GTK_DIALOG_MODAL,
            GTK_MESSAGE_INFO,
            GTK_BUTTONS_OK,
            "<b>Detected text:</b>\n\n%s", text);
    gtk_dialog_run(GTK_DIALOG(dialog));
    gtk_widget_destroy(dialog);
    free(text);
}

/*
** run the function to detect characters and inject these in the NN
** to get the text in the selected image
** display said text in a message window and write it in a file
** inputs :
**  - struct ImageInfo
**  - a boolean to check if the preprocessing should be automatic or manual
*/
void start(struct ImageInfo *info, char is_auto) {
    int gaussian_rad;

    SDL_FreeSurface(info->image);
    SDL_FreeSurface(info->image_framed);

    if (info->file_path != NULL &&
        g_ascii_strcasecmp (info->file_path, "") && // => str are not equals
        info->image) {
        // reload image to not blur it too much
        info->image = Load(info->file_path);

        // set needed var and do 1st binarization if auto mode
        if (is_auto) {
            gaussian_rad = 1;
            blackwhite(info->image);
        }
        else {
            gaussian_rad = info->guassian_blur_radius;
            binarize(info->image, info->binarize_threshold);
        }

        // preprocessing
        info->image = gaussian_blur(info->image, gaussian_rad);
        binarize(info->image, 127);
        info->image_framed = SDL_CreateRGBSurface(0,
                                                info->image->w,
                                                info->image->h,
                                                32,
                                                0,
                                                0,
                                                0,
                                                0);
        SDL_BlitSurface(info->image, NULL, info->image_framed, NULL);


        // reading the image
        struct CharList *char_list = text_framing(info->image,
                                                  info->image_framed);

        // resize the image if too big
        if (info->image_framed->h > MAX_IMAGE_HEIGHT &&
            info->image_framed->w > MAX_IMAGE_WIDTH) {
                double ratio =
                    MAX((double) MAX_IMAGE_WIDTH / (double) info->image_framed->h,
                        (double) MAX_IMAGE_HEIGHT / (double) info->image_framed->w);
                info->image_framed = resize_image(info->image_framed,
                                           (int) (ratio * (double) info->image_framed->h),
                                           (int) (ratio * (double) info->image_framed->w),
                                           true);
        }
        else if (info->image_framed->h > MAX_IMAGE_HEIGHT) {
            double ratio = (double) MAX_IMAGE_HEIGHT /
                                (double) info->image_framed->h;
            info->image_framed = resize_image(info->image_framed,
                                        (int) (ratio * (double) info->image_framed->h),
                                        (int) (ratio * (double) info->image_framed->w),
                                        true);
        }
        else if (info->image_framed->w > MAX_IMAGE_WIDTH) {
            double ratio = (double) MAX_IMAGE_WIDTH /
                                (double) info->image_framed->w;
            info->image_framed = resize_image(info->image_framed,
                                        (int) (ratio * (double) info->image_framed->h),
                                        (int) (ratio * (double) info->image_framed->w),
                                        true);
        }

        // draw the image
        gtk_widget_queue_draw(GTK_WIDGET(info->draw_area));

        // get the text in ascii
        get_nn_output(char_list, info->nn_file, info->nn_epochs);
        show_end_string(char_list);
        free_list(char_list);

        // reload image for future use
        info->image = Load(info->file_path);
    }
    else {
        // show error window asking user to select file
        show_no_file_err();
    }

}

/*
** redraws the image
** inputs :
**  - the widget that will be redrawn
**  - a cairo to draw on the widget
**  - pointer to a struct ImageInfo
** output :
** return TRUE to propagate the GTK signal
*/
gboolean on_draw(GtkWidget *widget, cairo_t *cr, gpointer user_data) {
    struct ImageInfo *info = user_data;
    Uint32 pixel;
    Uint8 r, g, b;

    if (info->image && info->image_framed) {
        // resize the image if too big
        if (info->image_framed->h > MAX_IMAGE_HEIGHT &&
            info->image_framed->w > MAX_IMAGE_WIDTH) {
                double ratio =
                    MAX((double) MAX_IMAGE_WIDTH / (double) info->image_framed->h,
                        (double) MAX_IMAGE_HEIGHT / (double) info->image_framed->w);
                info->image_framed = resize_image(info->image_framed,
                                           (int) (ratio * (double) info->image_framed->h),
                                           (int) (ratio * (double) info->image_framed->w),
                                           true);
        }
        else if (info->image_framed->h > MAX_IMAGE_HEIGHT) {
            double ratio = (double) MAX_IMAGE_HEIGHT /
                                (double) info->image_framed->h;
            info->image_framed = resize_image(info->image_framed,
                                        (int) (ratio * (double) info->image_framed->h),
                                        (int) (ratio * (double) info->image_framed->w),
                                        true);
        }
        else if (info->image_framed->w > MAX_IMAGE_WIDTH) {
            double ratio = (double) MAX_IMAGE_WIDTH /
                                (double) info->image_framed->w;
            info->image_framed = resize_image(info->image_framed,
                                        (int) (ratio * (double) info->image_framed->h),
                                        (int) (ratio * (double) info->image_framed->w),
                                        true);
        }

        // resize drawing area to let the image fit
        gtk_widget_set_size_request(widget,
                                    info->image_framed->w,
                                    info->image_framed->h);

        // display image
        for (int y = 0; y < info->image_framed->h; y++) {
            for (int x = 0; x < info->image_framed->w; x++) {
                pixel = get_pixel(info->image_framed, x, y);
                SDL_GetRGB(pixel, info->image_framed->format, &r, &g, &b);
                cairo_set_source_rgb(cr,
                                    (double) r / 255,
                                    (double) g / 255,
                                    (double) b / 255);
                cairo_rectangle(cr, x, y, 1, 1);
                cairo_fill(cr);
            }
        }
    }

    return FALSE;
}

/*
** get selected file in the gui
** inputs :
**  - the button that called the function
**  - pointer to a struct ImageInfo
** output :
** return TRUE to propagate the GTK signal
*/
gboolean on_file_set(GtkFileChooserButton *button, gpointer user_data) {
    struct ImageInfo *info = user_data;

    if (info->file_path) {
        free(info->file_path);
    }

    info->file_path = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(button));

    // loading the image
    if (info->image)
        free(info->image);
    if (info->image_framed)
        free(info->image_framed);
    info->image = Load(info->file_path);

    if (info->image) {

        // make a copy of the image to be displayed on the gui
        info->image_framed = SDL_CreateRGBSurface(0,
                                                info->image->w,
                                                info->image->h,
                                                32,
                                                0,
                                                0,
                                                0,
                                                0);
        SDL_BlitSurface(info->image, NULL, info->image_framed, NULL);
        info->guassian_blur_radius = 0;
        gtk_widget_set_sensitive(GTK_WIDGET(info->binarize_bt), TRUE);

        gtk_widget_queue_draw(GTK_WIDGET(info->draw_area));
    }
    else {
        // show error if no file is selected
        show_no_file_err();
    }
    return TRUE;
}

/*
** search save file
** inputs :
**  - pointer on button
**  - pointer to struct ImageInfo
** output:
** return TRUE to propagate the GTK signal
*/
gboolean on_nn_file_set(GtkFileChooserButton *button, gpointer user_data) {
    struct ImageInfo *info = user_data;

    // free previous string
    if (info->nn_file) {
        free(info->nn_file);
    }

    info->nn_file = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(button));
    gtk_widget_set_sensitive(GTK_WIDGET(info->nn_scale), FALSE);
    return TRUE;
}

/*
** change number of epochs with a slider
** inputs :
**  - pointer to struct ImageInfo
** output:
** return TRUE to propagate the GTK signal
*/
gboolean on_nn_epoch_change(GtkRange *range, gpointer user_data) {
    struct ImageInfo *info = user_data;
    info->nn_epochs = gtk_adjustment_get_value(info->nn_adj);
    return TRUE;
}

/*
** run start function with true in second arguement
** inputs :
**  - pointer on button
**  - pointer to struct ImageInfo
** output:
** return TRUE to propagate the GTK signal
*/
gboolean auto_data(GtkFileChooserButton *button, gpointer user_data) {
    struct ImageInfo *info = user_data;
    start(info, true);
    return TRUE;
}

/*
** change threshold value with a slider
** inputs :
**  - pointer to struct ImageInfo
** output:
** return TRUE to propagate the GTK signal
*/
gboolean on_bin_threshold_change(GtkRange *range, gpointer user_data) {
    struct ImageInfo *info = user_data;
    info->binarize_threshold = gtk_adjustment_get_value(info->adjustment);
    gtk_widget_set_sensitive(GTK_WIDGET(info->binarize_bt), TRUE);
    return TRUE;
}

/*
** apply binarization
** inputs :
**  - pointer on button
**  - pointer to struct ImageInfo
** output:
** return TRUE to propagate the GTK signal
*/
gboolean on_apply_bin(GtkButton *button, gpointer user_data) {
    struct ImageInfo *info = user_data;
    // redraw gui image if present
    if (info->file_path != NULL && g_ascii_strcasecmp (info->file_path, "")) {
        int width = info->image_framed->w;
        int height = info->image_framed->h;
        SDL_FreeSurface(info->image_framed);
        info->image_framed = resize_image(info->image, width, height, false);
        binarize(info->image_framed, info->binarize_threshold);
        gtk_widget_queue_draw(GTK_WIDGET(info->draw_area));
    }
    gtk_widget_set_sensitive(GTK_WIDGET(info->binarize_bt), FALSE);
    gtk_widget_set_sensitive(GTK_WIDGET(info->soft), TRUE);
    gtk_widget_set_sensitive(GTK_WIDGET(info->strong), TRUE);
    return TRUE;
}

/*
** apply a soft gaussian_blur if the button is pressed
** inputs :
**  - pointer on button
**  - pointer to struct ImageInfo
** output:
** return TRUE to propagate the GTK signal
*/
gboolean on_soft_guassian(GtkButton *button, gpointer user_data) {
    struct ImageInfo *info = user_data;
    info->guassian_blur_radius = 1;
    // redraw gui image if present
    if (info->file_path != NULL && g_ascii_strcasecmp (info->file_path, "")) {
        int width = info->image_framed->w;
        int height = info->image_framed->h;
        SDL_FreeSurface(info->image_framed);
        info->image_framed = resize_image(info->image, width, height, false);
        binarize(info->image_framed, info->binarize_threshold);
        info->image_framed = gaussian_blur(info->image_framed,
                                           info->guassian_blur_radius);
        binarize(info->image_framed, 128);
        gtk_widget_queue_draw(GTK_WIDGET(info->draw_area));
    }
    gtk_widget_set_sensitive(GTK_WIDGET(button), FALSE);
    gtk_widget_set_sensitive(GTK_WIDGET(info->strong), TRUE);
    return TRUE;
}

/*
** apply a strong gaussian_blur if the button is pressed
** inputs :
**  - pointer on button
**  - pointer to struct ImageInfo
** output:
** return TRUE to propagate the GTK signal
*/
gboolean on_strong_guassian(GtkButton *button, gpointer user_data) {
    struct ImageInfo *info = user_data;
    info->guassian_blur_radius = 2;
    // redraw gui image if present
    if (info->file_path != NULL && g_ascii_strcasecmp (info->file_path, "")) {
        int width = info->image_framed->w;
        int height = info->image_framed->h;
        SDL_FreeSurface(info->image_framed);
        info->image_framed = resize_image(info->image, width, height, false);
        binarize(info->image_framed, info->binarize_threshold);
        info->image_framed = gaussian_blur(info->image_framed,
                                           info->guassian_blur_radius);
        binarize(info->image_framed, 128);
        gtk_widget_queue_draw(GTK_WIDGET(info->draw_area));
    }
    gtk_widget_set_sensitive(GTK_WIDGET(button), FALSE);
    gtk_widget_set_sensitive(GTK_WIDGET(info->soft), TRUE);
    return TRUE;
}

/*
** run start function with false in second argument
** inputs :
**  - pointer on button
**  - pointer to struct ImageInfo
** output:
** return TRUE to propagate the GTK signal
*/
gboolean on_start(GtkButton *button, gpointer user_data) {
    struct ImageInfo *info = user_data;
    if (info->guassian_blur_radius != 0)
        start(info, false);
    return TRUE;
}
