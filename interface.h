#ifndef IMAGE_H
#define IMAGE_H

#include <gtk/gtk.h>

gboolean on_draw(GtkWidget *widget, cairo_t *cr, gpointer user_data);
gboolean on_file_set(GtkFileChooserButton *button, gpointer user_data);
gboolean on_nn_file_set(GtkFileChooserButton *button, gpointer user_data);
gboolean on_nn_epoch_change(GtkRange *range, gpointer user_data);
gboolean auto_data(GtkFileChooserButton *button, gpointer user_data);
gboolean on_bin_threshold_change(GtkRange *range, gpointer user_data);
gboolean on_apply_bin(GtkButton *button, gpointer user_data);
gboolean on_soft_guassian(GtkButton *button, gpointer user_data);
gboolean on_strong_guassian(GtkButton *button, gpointer user_data);
gboolean on_start(GtkButton *button, gpointer user_data);


#endif