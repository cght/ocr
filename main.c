#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "SDL/SDL.h"
#include "SDL/SDL_image.h"
#include <gtk/gtk.h>

#include "interface.h"
#include "image/sdl_management.h"
#include "image/tools.h"
#include "image/pixel_operations.h"

/*
** free ImageInfo memory
** input:
**  - image to free
*/
void free_image(struct ImageInfo img) {
    if (img.file_path)
        g_free(img.file_path);
    if (img.image)
        SDL_FreeSurface(img.image);
    if (img.image_framed)
        SDL_FreeSurface(img.image_framed);
    if (img.nn_file)
        g_free(img.nn_file);
    if (img.file_path)
        g_free(img.file_path);
}

int main() {
    // seed random number generator
    srand((long)time(NULL) * getpid() * 42);

    gtk_init(NULL, NULL);

    GtkBuilder *builder = gtk_builder_new();

    GError *error = NULL;
    if (gtk_builder_add_from_file(builder, "interface.glade", &error) == 0) {
        g_printerr("Error loading file: %s\n", error->message);
        g_clear_error(&error);
        return 1;
    }

    // main window
    GtkWindow *window = GTK_WINDOW(gtk_builder_get_object(builder,
                                                          "interface.window"));
    // area where the image will be displayed
    GtkDrawingArea *draw_area = GTK_DRAWING_AREA(gtk_builder_get_object(
                                                        builder,
                                                        "drawing.area"));
    // file chooser button  file.chooser
    GtkFileChooserButton *file_chooser = GTK_FILE_CHOOSER_BUTTON(
                            gtk_builder_get_object(builder, "file.chooser"));
    // NN settings
    GtkFileChooserButton *nn_chooser = GTK_FILE_CHOOSER_BUTTON(
                            gtk_builder_get_object(builder, "nn.load"));
    GtkScale *nn_scale = GTK_SCALE(gtk_builder_get_object(builder,
                                                          "nn.scale"));
    GtkAdjustment *nn_adj = GTK_ADJUSTMENT(gtk_builder_get_object(builder,
                                                                  "nn.adj"));
    // start buttons
    GtkButton *auto_button = GTK_BUTTON(gtk_builder_get_object(builder, "auto"));
    GtkButton *start_button = GTK_BUTTON(gtk_builder_get_object(builder, "start"));
    // binarization slider
    GtkScale *binarization_slider = GTK_SCALE(gtk_builder_get_object(builder,
                                                                     "scale"));
    GtkAdjustment *binarization_adjustment = GTK_ADJUSTMENT(
                                gtk_builder_get_object(builder,"adjustment"));
    GtkButton *binarization_button = GTK_BUTTON(
                                    gtk_builder_get_object(builder,"binarize"));
    // noise reduction buttons
    GtkButton *soft_button = GTK_BUTTON(gtk_builder_get_object(builder, "soft"));
    GtkButton *strong_button = GTK_BUTTON(gtk_builder_get_object(builder,
                                                                 "strong"));
    // exit button
    GtkButton *exit_button = GTK_BUTTON(gtk_builder_get_object(builder, "quit"));

    // create necessary data
    struct ImageInfo info = {
        .file_path = NULL,
        .nn_file = NULL,
        .nn_adj = nn_adj,
        .nn_scale = nn_scale,
        .nn_epochs = gtk_adjustment_get_value(nn_adj),
        .image = NULL,
        .image_framed = NULL,
        .draw_area = draw_area,
        .adjustment = binarization_adjustment,
        .guassian_blur_radius = 0,
        .binarize_threshold = gtk_adjustment_get_value(binarization_adjustment),
        .soft = soft_button,
        .strong = strong_button,
        .binarize_bt = binarization_button,
    };

    // Connects event handlers.
    g_signal_connect(window, "destroy", G_CALLBACK(gtk_main_quit), NULL);
    g_signal_connect(draw_area, "draw", G_CALLBACK(on_draw), &info);
    g_signal_connect(exit_button, "clicked", G_CALLBACK(gtk_main_quit), NULL);
    g_signal_connect(file_chooser, "file-set", G_CALLBACK(on_file_set), &info);
    g_signal_connect(nn_chooser, "file-set", G_CALLBACK(on_nn_file_set), &info);
    g_signal_connect(auto_button,
                     "clicked",
                     G_CALLBACK(auto_data),
                     &info);
    g_signal_connect(binarization_slider,
                     "value-changed",
                     G_CALLBACK(on_bin_threshold_change),
                     &info);
    g_signal_connect(soft_button,
                     "clicked",
                     G_CALLBACK(on_soft_guassian),
                     &info);
    g_signal_connect(strong_button,
                     "clicked",
                     G_CALLBACK(on_strong_guassian),
                     &info);
    g_signal_connect(start_button,
                     "clicked",
                     G_CALLBACK(on_start),
                     &info);
    g_signal_connect(binarization_button,
                     "clicked",
                     G_CALLBACK(on_apply_bin),
                     &info);
    g_signal_connect(nn_scale,
                     "value-changed",
                     G_CALLBACK(on_nn_epoch_change),
                     &info);

    gtk_widget_set_size_request(GTK_WIDGET(draw_area), 1, 1);
    gtk_widget_set_sensitive(GTK_WIDGET(soft_button), FALSE);
    gtk_widget_set_sensitive(GTK_WIDGET(strong_button), FALSE);
    gtk_widget_set_sensitive(GTK_WIDGET(binarization_button), FALSE);

    gtk_main();

    free_image(info);

    return 0;
}
