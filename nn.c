#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include "image/tools.h"
#include "image/detect_char.h"
#include "image/sdl_management.h"
#include "image/preprocessing.h"
#include "nn/loading.h"
#include "nn/neuronal_network.h"
#include "nn/saving.h"
#include "nn/training.h"
#include "train_load.h"

/*
** strrev2
** reimplementation of strrev (string.h)
** which is not present on all platforms
** reverse str
*/
static inline
char *strrev2(char *str)
{
    char *p1, *p2;

    if (! str || ! *str)
        return str;
    for (p1 = str, p2 = str + strlen(str) - 1; p2 > p1; ++p1, --p2)
    {
        *p1 ^= *p2;
        *p2 ^= *p1;
        *p1 ^= *p2;
    }
    return str;
}

/*
** get_nn_output
** fill res value of each element of char_list
** with character detected by network
** if save_path is NULL, network will be trained for given epochs
** otherwise, save_path will be loaded and used for feedforward
*/
void get_nn_output(struct CharList *char_list, char *save_path, size_t epochs)
{
    Network net = init_network(1);

    if (save_path)
    {
        network_loading(&net, save_path);
    }
    else
    {
        TrainingList *t_list = make_training_list();

        printf("- TRAINING - %li epochs selected.\n", epochs);

        train(&net, t_list, epochs);

        free_training(t_list);
    }

    network_save(&net);

    for (; char_list; char_list = char_list->next)
    {
        // if result character is already initialized, skip nn
        if (char_list->value->res != 0)
        {
            continue;
        }

        char *ptr = get_image_data(char_list->value);

        char_list->value->res = get_recognized_character(&net, ptr);

        free(ptr);
    }
}
