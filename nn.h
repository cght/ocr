#ifndef NN_H
#define NN_H

#include "image/tools.h"

void get_nn_output(struct CharList *char_list, char *save_path, size_t epochs);

#endif
