//IMPORTS----------------------------------------------------------------------/
#include <stdio.h>
#include <stdlib.h>
//HEADERS----------------------------------------------------------------------/
#include "display.h"
#include "neuronal_network.h"

/*
** display
** display the whole network
** input:
**  - pointer on network to display
*/
void display_network(Network *net)
{
    printf("\n=========hidden==========\n\n");
    for (int i = 0; i < NUMBER_HIDDEN; i++)
    {
        Neuron h = net->hidden[i];
        printf("h%i | weights = ",i);
        for (int j = 0; j < NUMBER_INPUTS; j++)
        {
            printf("[%lf]", h.weights[j]);
        }
        printf("| bias = %lf\n", h.bias);
    }
    printf("\n=========outputs==========\n\n");
    for (int i = 0; i < NUMBER_OUTPUTS; i++)
    {
        Neuron o = net->outputs[i];
        printf("o%i | weights = ",i);
        for (int j = 0; j < NUMBER_HIDDEN; j++)
        {
            printf("[%lf]", o.weights[j]);
        }
        printf("| bias = %lf\n", o.bias);
    }
    printf("\n");
}
