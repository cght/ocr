//IMPORTS----------------------------------------------------------------------/
#include <stdio.h>
#include <stdlib.h>
#include <err.h>
//HEADERS----------------------------------------------------------------------/
#include "loading.h"
#include "neuronal_network.h"


/*
** network_loading
** load the weighs and biases for all neurons from a file
** input:
**  - pointer on network that will be loaded
*/
void network_loading(Network *net, char *path)
{
    FILE* file = fopen(path, "r");
    if (file != NULL)
    {
        double numb;

        for (int i = 0; i < NUMBER_HIDDEN; i++)
        {
            for (int j = 0; j < NUMBER_INPUTS + 1; j++)
            {
                fscanf(file,"%lf", &numb);
                net->hidden[i].weights[j] = numb;
            }
            net->hidden[i].bias = numb;
         }

         for (int i = 0; i < NUMBER_OUTPUTS; i++)
         {
             for (int j = 0; j < NUMBER_HIDDEN + 1; j++)
             {
                 fscanf(file,"%lf", &numb);
                 net->outputs[i].weights[j] = numb;
             }
             net->outputs[i].bias = numb;
          }
          fclose(file);
    }
    else
    {
        errx(1, "Please enter a valid path for loading");
    }
}
