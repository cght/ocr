//IMPORTS----------------------------------------------------------------------/
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
//HEADERS----------------------------------------------------------------------/
#include "utils.h"
#include "neuronal_network.h"
#include "saving.h"
#include "display.h"
#include "loading.h"

int main()
{
    // seed random number generator
    srand((long)time(NULL) * getpid() * 42);

    double inputs[NUMBER_INPUTS] = { 0 };

    // make random image
    for (size_t i = 0; i < NUMBER_INPUTS; i++)
    {
        if (random_1() > 0.5)
        {
            inputs[i] = 1;
        }
        else
        {
            inputs[i] = 0;
        }
    }

    Network net = init_network(1);

    double result[NUMBER_OUTPUTS];

    feedforward(&net, inputs, result);

    for (size_t i = 0; i < NUMBER_OUTPUTS; i++)
    {
        printf("%f\n", result[i]);
    }
    printf("\n");

    printf("Detected character: %c\n", get_recognized_character(&net, inputs));

    return 0;
}
