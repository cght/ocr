//IMPORTS----------------------------------------------------------------------/
#include <stdio.h>
#include <stdlib.h>
//HEADERS----------------------------------------------------------------------/
#include "utils.h"
#include "neuronal_network.h"


/*
** feedforward
** feeds data forward through the network
** input:
**  - network
**  - inputs array
**  - outputs values array (les valeurs des outputs)
*/
void feedforward(Network *net, char inputs[], double result[])
{
    double out_hidden[NUMBER_HIDDEN];
    for (int i = 0; i < NUMBER_HIDDEN; i++)
    {
        Neuron h = net->hidden[i];
        double total = h.bias;
        for (int j = 0; j < NUMBER_INPUTS; j++)
        {
            total += h.weights[j] * inputs[j];
        }
        double sig = sigmoid(total);
        out_hidden[i] = sig;
    }

    for (int i = 0; i < NUMBER_OUTPUTS; i++)
    {
        Neuron o = net->outputs[i];
        double total = o.bias;
        for (int j = 0; j < NUMBER_HIDDEN; j++)
        {
            total += o.weights[j] * out_hidden[j];
        }
        double sig = sigmoid(total);
        result[i] = sig;
    }
}

/*
** get_recognized_character
** return character with highest score using feedforward
*/
char get_recognized_character(Network *net, char image[])
{
    // list of characters supported by the network
    char output_characters[NUMBER_OUTPUTS] =
    {
        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
        'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
        'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'
        // '.', ',', '!', '?', '-', ';', ':', '(', ')', '&'
    };

    double output[NUMBER_OUTPUTS] = {0};

    feedforward(net, image, output);

    size_t index = array_max(output, NUMBER_OUTPUTS);

    return output_characters[index];
}

/*
** init_neuron
** initialize a neuron from the hidden layer with weights and bias
** weights and biases are set to random doubles
** output:
**  - neuron struct with all values initialized
*/
Neuron init_hidden_neuron()
{
    Neuron neuron;
    for (int i = 0; i < NUMBER_INPUTS; i++)
    {
        neuron.weights[i] = random_1() - 0.5;
    }
    neuron.bias = random_1();
    return neuron;
}

/*
** init_neuron
** initialize an output neuron with weights and bias
** weights and biases are set to random doubles
** output:
**  - neuron struct with all values initialized
*/
Neuron init_output_neuron()
{
    Neuron neuron;
    for (int i = 0; i < NUMBER_HIDDEN; i++)
    {
        neuron.weights[i] = random_1() - 0.5;
    }
    neuron.bias = random_1();
    return neuron;
}

/*
** init_neuron
** initialize an output neuron with weights and bias
** weights and biases are set to random doubles
** inputs :
**  - inputs of the layer initialized
** output:
**  - neuron struct with all values initialized
*/
Neuron init_empty(int previous_layer)
{
    Neuron neuron;
    for (int i = 0; i < previous_layer; i++)
    {
        neuron.weights[i] = 0;
    }
    neuron.bias = 0;
    return neuron;
}

/*
** init_network
** initialize a network with weights and bias
** inputs :
**  - bool determines whether to randomize weights
** output:
**  - network struct with all neurons initialized
*/
Network init_network(int bool)
{
    Network net;

    if (bool == 1)
    {
        for (int i = 0; i < NUMBER_HIDDEN; i++)
        {
            net.hidden[i] = init_hidden_neuron();
        }
        for (int i = 0; i < NUMBER_OUTPUTS; i++)
        {
            net.outputs[i] = init_output_neuron();
        }
    }
    else
    {
        for (int i = 0; i < NUMBER_HIDDEN; i++)
        {
            net.hidden[i] = init_empty(NUMBER_INPUTS);
        }
        for (int i = 0; i < NUMBER_OUTPUTS; i++)
        {
            net.outputs[i] = init_empty(NUMBER_HIDDEN);
        }
    }

    return net;
}
