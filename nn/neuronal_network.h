#ifndef NEURONAL_NETWORK_H
#define NEURONAL_NETWORK_H

#define NUMBER_INPUTS 169
#define NUMBER_HIDDEN 62
#define NUMBER_OUTPUTS 62

typedef struct Neuron
{
    double weights[NUMBER_INPUTS];
    double bias;
} Neuron;

typedef struct Network
{
    Neuron hidden[NUMBER_HIDDEN];
    Neuron outputs[NUMBER_OUTPUTS];
} Network;

void feedforward(Network *net, char inputs[], double result[]);

char get_recognized_character(Network *net, char image[]);

Neuron init_hidden_neuron();

Neuron init_output_neuron();

Neuron init_empty(int layer);

Network init_network(int bool);

#endif
