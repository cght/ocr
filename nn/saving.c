//IMPORTS----------------------------------------------------------------------/
#include <stdio.h>
#include <stdlib.h>
#include <err.h>
//HEADERS----------------------------------------------------------------------/
#include "saving.h"
#include "neuronal_network.h"

/*
** network_save
** save the weighs and biases for all neurons to a file
** input:
**  - pointer on network to save
*/
void network_save(Network *net)
{
    FILE *file = NULL;
    file = fopen("save", "w"); //ne pas mettre le .txt
    if (file != NULL)
    {
        for (int i = 0; i < NUMBER_HIDDEN; i++)
        {
            Neuron h = net->hidden[i];
            for (int j = 0; j < NUMBER_INPUTS; j++)
            {
                fprintf(file, "%lf ", h.weights[j]);
            }
            fprintf(file, "%lf\n", h.bias);
        }
        for (int i = 0; i < NUMBER_OUTPUTS; i++)
        {
            Neuron o = net->outputs[i];
            for (int j = 0; j < NUMBER_HIDDEN; j++)
            {
                fprintf(file, "%lf ", o.weights[j]);
            }
            fprintf(file, "%lf\n", o.bias);
        }
        fclose(file);
    }
    else
    {
        errx(1, "Please enter a valid path for saving");
    }
}
