//IMPORTS----------------------------------------------------------------------/
#include <stdio.h>
#include <stdlib.h>
#include <err.h>
//HEADERS----------------------------------------------------------------------/
#include "utils.h"
#include "training.h"
#include "neuronal_network.h"

/*
** train
** train the neural network
** input:
**  - pointer on network to use
**  - training list
**  - number of iterations (epochs) to train the network for
*/
void train(Network *net,
           TrainingList *training,
           size_t train_number)
{
    // loop for each epoch
    for (size_t epoch = 0; epoch < train_number; epoch++)
    {
        printf("training epoch %zu\n", epoch);

        // loop over each training matrix
        for (TrainingList *tl = training; tl; tl = tl->next)
        {
            // compute outputs of hidden layer
            double sum_h[NUMBER_HIDDEN] = {0};
            double h[NUMBER_HIDDEN] = {0};

            for (size_t i = 0; i < NUMBER_HIDDEN; i++)
            {
                for (size_t w_num = 0; w_num < NUMBER_INPUTS; w_num++)
                {
                    sum_h[i] += net->hidden[i].weights[w_num]
                              * tl->data[w_num];
                }
                sum_h[i] += net->hidden[i].bias;
                h[i] = sigmoid(sum_h[i]);
            }

            // compute end outputs
            double sum_o[NUMBER_OUTPUTS] = {0};
            double o[NUMBER_OUTPUTS] = {0};

            for (size_t i = 0; i < NUMBER_OUTPUTS; i++)
            {
                for (size_t h_num = 0; h_num < NUMBER_HIDDEN; h_num++)
                {
                    sum_o[i] += net->outputs[i].weights[h_num] * h[h_num];
                }
                sum_o[i] += net->outputs[i].bias;
                o[i] = sigmoid(sum_o[i]);
            }

            // derivative of loss by output
            double dLdo[NUMBER_OUTPUTS] = {0};

            for (size_t i = 0; i < NUMBER_OUTPUTS; i++)
            {
                dLdo[i] = -2 * (tl->correct_output[i] - o[i]);
            }

            // derivative of output by hidden weights
            double dodw[NUMBER_OUTPUTS * NUMBER_HIDDEN] = {0};
            double dodb[NUMBER_OUTPUTS] = {0};

            for (size_t i = 0; i < NUMBER_OUTPUTS; i++)
            {
                for (size_t j = 0; j < NUMBER_HIDDEN; j++)
                {
                    dodw[i * NUMBER_HIDDEN + j] = h[j]
                                                * deriv_sigmoid(sum_o[i]);
                }
                dodb[i] = deriv_sigmoid(sum_o[i]);
            }

            // derivative of output by hidden outputs
            double dodh[NUMBER_OUTPUTS * NUMBER_HIDDEN] = {0};

            for (size_t i = 0; i < NUMBER_OUTPUTS; i++)
            {
                for (size_t j = 0; j < NUMBER_HIDDEN; j++)
                {
                    dodh[i * NUMBER_HIDDEN + j] = net->outputs[i].weights[j]
                                                * deriv_sigmoid(sum_o[i]);
                }
            }

            // derivative of hidden by input
            double dhdw[NUMBER_HIDDEN * NUMBER_INPUTS] = {0};
            double dhdb[NUMBER_HIDDEN] = {0};

            for (size_t i = 0; i < NUMBER_HIDDEN; i++)
            {
                for (size_t j = 0; j < NUMBER_INPUTS; j++)
                {
                    dhdw[i * NUMBER_INPUTS + j] = tl->data[j]
                                                * deriv_sigmoid(sum_h[i]);
                }
                dhdb[i] = deriv_sigmoid(sum_h[i]);
            }

            // update hidden weights + biases
            for (size_t i = 0; i < NUMBER_HIDDEN; i++)
            {
                for (size_t j = 0; j < NUMBER_INPUTS; j++)
                {
                    for (size_t k = 0; k < NUMBER_OUTPUTS; k++)
                    {
                        net->hidden[i].weights[j] -= LEARN_RATE
                                                   * dLdo[k]
                                                   * dodh[k * NUMBER_HIDDEN + i]
                                                   * dhdw[i * NUMBER_INPUTS + j];
                    }
                }
                for (size_t k = 0; k < NUMBER_OUTPUTS; k++)
                {
                    net->hidden[i].bias -= LEARN_RATE
                                         * dLdo[k]
                                         * dodh[k * NUMBER_HIDDEN + i]
                                         * dhdb[i];
                }
            }

            // update output weights + biases
            for (size_t i = 0; i < NUMBER_OUTPUTS; i++)
            {
                for (size_t j = 0; j < NUMBER_HIDDEN; j++)
                {
                    net->outputs[i].weights[j] -= LEARN_RATE
                                                * dLdo[i]
                                                * dodw[i * NUMBER_HIDDEN + j];
                }
                net->outputs[i].bias -= LEARN_RATE
                                      * dLdo[i]
                                      * dodb[i];
            }
        }
    }
}


// UTILS FOR DYNAMIC TRAINING LIST

/*
** free_training
** free all elements of training list
*/
void free_training(TrainingList *list)
{
    if (list->next)
    {
        free_training(list->next);
    }
    free(list);
}

/*
** training_append
** append element to dynamic training list
*/
void training_append(TrainingList **list_ptr,
                     char data[],
                     char correct_output[])
{
    TrainingList *new_elm = malloc(sizeof (TrainingList));

    if (!new_elm)
    {
        errx(1, "training_append: malloc failed on new_elm");
    }

    new_elm->next = NULL;
    for (size_t i = 0; i < NUMBER_INPUTS; i++)
    {
        new_elm->data[i] = data[i];
    }
    for (size_t i = 0; i < NUMBER_OUTPUTS; i++)
    {
        new_elm->correct_output[i] = correct_output[i];
    }

    // append to empty list
    if (*list_ptr == NULL)
    {
        *list_ptr = new_elm;
    }
    else
    {
        TrainingList *list = *list_ptr;

        for (; list->next; list = list->next) {}

        list->next = new_elm;
    }
}
