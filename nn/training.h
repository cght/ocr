#ifndef TRAINING_H
#define TRAINING_H

#include <stdlib.h>
#include "neuronal_network.h"

#define LEARN_RATE 1.0

// dynamic list for nn training
typedef struct TrainingList
{
    struct TrainingList *next;
    char data[NUMBER_INPUTS];
    char correct_output[NUMBER_OUTPUTS];
} TrainingList;

void train(
    Network *net,
    TrainingList *training,
    size_t train_number
);

void free_training(TrainingList *list);

void training_append(
    TrainingList **list_ptr,
    char data[],
    char correct_output[]
);

#endif
