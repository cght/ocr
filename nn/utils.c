#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "training.h"

/*
** sigmoid
** sigmoid function
*/
double sigmoid(double x)
{
    return ((double)1 / (1 + exp(-x)));
}

/*
** deriv_sigmoid
** derivative of sigmoid function
*/
double deriv_sigmoid(double x)
{
    double sigx = sigmoid(x);
    return sigx * (1 - sigx);
}

/*
** mse_loss
** MSE (Mean Squared Error) function
** input:
**  - expected numbers
**  - actual numbers given
** ouput:
** loss of given numbers compared to expected result
*/
double mse_loss(double expected_list[], double actual_list[], size_t length)
{
    double sum = 0;
    size_t i = 0;

    while (i < length)
    {
        double temp = actual_list[i] - expected_list[i];
        sum += temp * temp;
        i++;
    }

    return sum / length;
}

/*
** random_1
** random double between 0 and 1
*/
double random_1()
{
    return (double)rand()/RAND_MAX;
}

/*
** find_index
** find x in array and return index (-1 if not found)
*/
int find_index(char x, char array[], size_t length)
{
    int index = -1;

    for (size_t i = 0; index == -1 && i < length; i++)
    {
        if (array[i] == x)
        {
            index = i;
        }
    }

    return index;
}

/*
** list_max
** return index of largest value in array
*/
size_t array_max(double array[], size_t length)
{
    double max = array[0];
    size_t index = 0;

    for (size_t i = 1; i < length; i++)
    {
        if (array[i] > max)
        {
            max = array[i];
            index = i;
        }
    }

    return index;
}
