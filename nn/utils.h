#include <stdlib.h>

#ifndef UTILS_H
#define UTILS_H

double sigmoid(double x);

double deriv_sigmoid(double x);

double mse_loss(double *expected_list, double *actual_list, size_t length);

double random_1();

int find_index(char x, char array[], size_t length);

size_t array_max(double array[], size_t length);

#endif
