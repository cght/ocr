#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <err.h>
#include "SDL/SDL.h"
#include "SDL/SDL_image.h"

#include "image/detect_char.h"
#include "image/sdl_management.h"
#include "image/preprocessing.h"
#include "image/tools.h"

#include "nn/loading.h"
#include "nn/neuronal_network.h"
#include "nn/saving.h"
#include "nn/training.h"

#include "train_load.h"
#include "nn.h"

#define EPOCHS 30000


int main(int argc, char** argv)
{
    if(argc < 2)
    {
        errx(1, "Please enter a valid image.");
    }
    char* param = argv[1];

    // seed random number generator
    srand((long)time(NULL) * getpid() * 42);

    // loading the image
    SDL_Surface* image = Load(param);

    // preprocessing
    blackwhite(image);
    image = gaussian_blur(image, 1);
    binarize(image, 127);

    // read the image
    SDL_Surface *image_cp = SDL_CreateRGBSurface(0,
                                                 image->w,
                                                 image->h,
                                                 32,
                                                 0,
                                                 0,
                                                 0,
                                                 0);
    SDL_BlitSurface(image, NULL, image_cp, NULL);
    struct CharList *char_list = text_framing(image, image_cp);

    get_nn_output(char_list, NULL, EPOCHS);

    char_list = revert_list(char_list);
    gchar *text = malloc(sizeof(char) * (length_list(char_list) + 1));
    if (!text)
    {
        errx(1, "main: malloc failed on text");
    }
    size_t i = 0;
    for (; char_list; char_list = char_list->next) {
        text[i] = char_list->value->res;
        i++;
    }
    text[i] = 0;
    printf("%s\n", text);

    char out = 0;
    char alpha[64] =
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ\nabcdefghjiklmnopqrstuvwxyz\n0123456789";

    for (size_t i = 0; i < 64; i++) {
        if (text[i] != alpha[i]) {
            out = 1;
            break;
        }
    }

    // free memory
    free_list(char_list);
    free(text);
    free_list(char_list);
    SDL_FreeSurface(image);
    SDL_FreeSurface(image_cp);
    SDL_Quit();

    if (out == 1) {
        errx(1, "Not the one\n");
    }

    return 0;
}
