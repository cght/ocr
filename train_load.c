#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "image/create_lines.h"
#include "image/detect_char.h"
#include "image/sdl_management.h"
#include "image/pixel_operations.h"
#include "image/preprocessing.h"
#include "image/tools.h"

#include "nn/neuronal_network.h"
#include "nn/training.h"
#include "nn/utils.h"

#define IMG_PER_DIR 6

/*
** exists
** return whether file exists
*/
static inline
int exists(const char *fname)
{
    FILE *file;
    if ((file = fopen(fname, "r")))
    {
        fclose(file);
        return 1;
    }
    return 0;
}

/*
** make_training_list
** create training list from image database
** with IMG_PER_DIR images per folder
*/
TrainingList *make_training_list()
{
    TrainingList *t_list = NULL;

    // loop over each character folder
    for (size_t dir = 0; dir < NUMBER_OUTPUTS; dir++)
    {
        // loop over each image index
        for (size_t img = 0; img < IMG_PER_DIR; img++)
        {
            char path[128];
            char dir_str[8];
            char img_str[8];
            snprintf(dir_str, sizeof dir_str, "%zu", dir);
            snprintf(img_str, sizeof img_str, "%zu", img);

            // create string with current image path
            strcpy(path, "db/");
            strcat(path, dir_str);
            strcat(path, "/");
            strcat(path, img_str);
            strcat(path, ".jpg");

            // ignore current image if not present
            if (!exists(path))
            {
                continue;
            }

            printf("appending %zu/%zu\n", dir, img);

            // load the image
            SDL_Surface* image = Load(path);

            // binarize once
            blackwhite(image);
            // no need for noise reduction, since training images have none

            // copy image for text framing
            SDL_Surface *image_cp =
                SDL_CreateRGBSurface(0, image->w, image->h, 32, 0, 0, 0, 0);

            // create list of character matrices
            struct CharList *charList = text_framing(image, image_cp);

            // create list of expected output:
            // 0 for all characters except current one, set to 1
            char correct_output[NUMBER_OUTPUTS] = {0};
            correct_output[dir] = 1;

            // loop over all detected characters
            // (to allow support for multiple characters per training image)
            for (; charList; charList = charList->next) {
                char *ptr = get_image_data(charList->value);

                training_append(&t_list, ptr, correct_output);

                free(ptr);
            }

            // free memory and close SDL
            free_list(charList);
            SDL_FreeSurface(image);
            SDL_FreeSurface(image_cp);
            SDL_Quit();
        }
    }

    return t_list;
}
